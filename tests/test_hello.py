from helloworld_mve import hello

def test_hello():
    assert hello.say() == "Hello World"
    assert hello.say() != "Goodbye World"
    assert hello.say() != "hello world"

